//
//  ZHMainBottomColumnModel.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/9.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHMainBottomColumnModel: NSObject {

    /*
     "banner_image_url": "http://img03.liwushuo.com/image/160708/or81k6fea.jpg-w300",
     "category": "礼物",
     "cover_image_url": "http://img02.liwushuo.com/image/160720/xdt9kjriy.jpg-w720",
     "created_at": 1467970933,
     "description": "会买不一定“惠”买，每日10款超低价，举双手奉上。——from亚马逊良心出品",
     "id": 90,
     "order": 0,
     "post_published_at": 1473292800,
     "status": 0,
     "subtitle": "",
     "title": "省钱大总攻",
     "updated_at": 1473326482
     */
    var category : String?  //类别
    var title : String?     //标题
    
    
    init(dict : [String : AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
    }
}
