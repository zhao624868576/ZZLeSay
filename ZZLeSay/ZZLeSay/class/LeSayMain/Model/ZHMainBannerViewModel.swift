//
//  ZHMainBannerViewModel.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/8.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHMainBannerViewModel: NSObject {

    /*
     "ad_monitors": [ ],
     "channel": "all",
     "id": 714,
     "image_url": "http://img02.liwushuo.com/image/160907/ps21v1bzo.jpg-w720",
     "order": 350,
     "status": 0,
     "target_id": 1045462,
     "target_type": "url",
     "target_url": "liwushuo:///page?page_action=navigation&login=false&type=post&post_id=1045462",
     "type": "post",
     "webp_url": "http://img02.liwushuo.com/image/160907/ps21v1bzo.jpg?imageView2/2/w/720/q/85/format/webp"
     */
    var id = 0
    var order = 0
    var image_url : NSURL?
    var target_url : String?
    var webp_url : String?
    
    init(dict:[String:AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
    }

}
