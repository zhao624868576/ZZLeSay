//
//  ZHMainBottomModel.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/9.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHMainBottomModel: NSObject {

    /*
     "ad_monitors": [ ],
     "author": {},
     "column": {},
     "content_type": 1,
     "content_url": "http://www.liwushuo.com/posts/1045646/content",
     "cover_image_url": "http://img03.liwushuo.com/image/160908/stmuoqwz5.jpg-w720",
     "cover_webp_url": "http://img03.liwushuo.com/image/160908/stmuoqwz5.jpg?imageView2/2/w/720/q/85/format/webp",
     "created_at": 1473379200,
     "editor_id": 1045,
     "feature_list": [ ],
     "hidden_cover_image": false,
     "id": 1045646,
     "introduction": "嚷嚷着一整个夏天要减肥的小盆友们，终于可以敞开了吃肉了~立秋了，天气变得凉爽，连胃口都变好了，当然要正儿八经的贴个秋膘，这样冬天寒风来袭才不会那么冷。比起出去挥霍，还是自己动手比较合口味又省钱，置办些厨房神器，无肉不欢的小伙伴，我们一起来贴个秋膘呗~",
     "labels": [ ],
     "liked": false,
     "likes_count": 227,
     "limit_end_at": null,
     "published_at": 1473379200,
     "share_msg": "嚷嚷着一整个夏天要减肥的小盆友们，终于可以敞开了吃肉了~立秋了，天气变得凉爽，连胃口都变好了，当然要正儿八经的贴个秋膘，这样冬天寒风来袭才不会那么冷。比起出去挥霍，还是自己动手比较合口味又省钱，置办些厨房神器，无肉不欢的小伙伴，我们一起来贴个秋膘呗~",
     "short_title": "",
     "status": 0,
     "template": "",
     "title": "第62期 | 贴秋膘咯！厨房神器让你吃得更香",
     "type": "post",
     "updated_at": 1473331314,
     "url": "http://www.liwushuo.com/posts/1045646"
     */
    
    var autherModel : ZHMainBottomAuthorModel?     //商家信息
    var columnModel : ZHMainBottomColumnModel?     //商家类别信息
    
    var likes_count : NSNumber?               //喜欢数
    var title : String?                       //底部标题
    var cover_image_url : NSURL?              //内容背景图片
    var share_msg : String?                   //分享描述信息
    var url : String?                          //点击加载h5页面
    
    init(dict : [String : AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
        if let author = dict["author"] as? [String : AnyObject] {
            autherModel = ZHMainBottomAuthorModel(dict: author)
        }
        if let column = dict["column"] as? [String : AnyObject] {
            columnModel = ZHMainBottomColumnModel(dict: column)
        }
        
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
    }
    
    
    
    
    
    
}
