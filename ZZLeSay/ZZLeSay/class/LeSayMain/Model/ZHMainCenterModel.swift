//
//  ZHMainCenterModel.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/8.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHMainCenterModel: NSObject {

    /*
     "ad_monitors": [ ],
     "id": 59,
     "image_url": "http://img02.liwushuo.com/image/160903/4s9vk2gjf.jpg-w720",
     "target_url": "liwushuo:///page?type=url&url=http%3A%2F%2Fwww.liwushuo.com%2Fsubcategories%2F195%2Fitems",
     "webp_url": "http://img02.liwushuo.com/image/160903/4s9vk2gjf.jpg?imageView2/2/w/720/q/85/format/webp"
     */
    
    var id = 0
    var image_url : NSURL?
    var target_url : String?
    var webp_url : String?
    
    init(dict:[String:AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
    }
    
    
    
    
}
