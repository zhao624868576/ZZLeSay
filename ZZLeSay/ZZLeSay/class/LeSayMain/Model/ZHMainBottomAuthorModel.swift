//
//  ZHMainBottomAuthorModel.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/9.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHMainBottomAuthorModel: NSObject {

    /*
     "avatar_url": "http://img01.liwushuo.com/image/160708/yactcsfo5.jpg",
     "avatar_webp_url": null,
     "created_at": 1467971922,
     "id": 35,
     "nickname": "凹凸曼"
     */
    
    var avatar_url : NSURL?  //图像url
    var nickname : String?   //昵称
    
    
    init(dict : [String : AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
    }
}
