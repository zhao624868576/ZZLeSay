//
//  ZHMainWebVController.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/9.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit
import SVProgressHUD

class ZHMainWebVController: UIViewController {

    var jsurl : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = NSURL(string: jsurl!)
        let webView = UIWebView(frame: self.view.bounds)
        webView.delegate = self;
        webView.loadRequest(NSURLRequest(URL: url!))
        self.view.addSubview(webView)
    }
}
extension ZHMainWebVController:UIWebViewDelegate{
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool{
        return true
    }
    
    func webViewDidStartLoad(webView: UIWebView){
        
        SVProgressHUD.show()
    }
    
    func webViewDidFinishLoad(webView: UIWebView){
    
        SVProgressHUD.dismiss()
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?){
    
        SVProgressHUD.showErrorWithStatus("网络不给力,请稍后再试...")
    }
}