//
//  ZHContentVC.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/8.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHContentVC: UIViewController {
    
    //中心数据
    var centerDataArray = [ZHMainCenterModel]() {
        didSet{
        bannerCollecView.dataArray = centerDataArray
        }
    }
    //banner数据
    var bannerDataArray = [ZHMainBannerViewModel]() {
        didSet{
            bannerScrollView.dataArray = bannerDataArray
        }
    }
    //底部内容数据
    var DataArray = [ZHMainBottomModel]() {
        didSet{
           contentTableView.reloadData()
        }
    }
    
    var bannerTempArray = [ZHMainBannerViewModel]()
    private let cellID = "mainCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentTableView)
        contentTableView.frame = self.view.bounds
        contentTableView.tableHeaderView = tabView
        //  支持自适应 cell
        contentTableView.estimatedRowHeight = 245;
        contentTableView.rowHeight = UITableViewAutomaticDimension;
        
        tabView.addSubview(bannerScrollView)
        tabView.addSubview(bannerCollecView)
        
        contentTableView.registerNib(UINib.init(nibName: "ZHMainTableViewCell", bundle: nil), forCellReuseIdentifier: cellID)
        //网络请求
        self.loadNetData()
    }
    func loadNetData() {
        ZHNetworkTools.shareInstance.loadBannerViewData { (result, error) in
            if error != nil{ return }
            for dict in result!{
                let model = ZHMainBannerViewModel(dict: dict)
                self.bannerTempArray.append(model)
            }
            self.bannerDataArray = self.bannerTempArray
        }
        ZHNetworkTools.shareInstance.loadMainCenterContentData { (result, error) in
            if error != nil{
                return
            }
            for dict in result!{
                let model = ZHMainCenterModel(dict: dict)
                self.centerDataArray.append(model)
            }
        }
        ZHNetworkTools.shareInstance.loadMainContentData { (result, error) in
            if error != nil{return}
            for dict in result!{
                let model = ZHMainBottomModel(dict: dict)
                self.DataArray.append(model)
            }
        }
    }
    //广告位视图
    lazy var bannerScrollView : ZHBannerView = {
        let banner = ZHBannerView()
        banner.frame = CGRectMake(0, 0, kScreenWidth, 175)
        return banner
    }()
    lazy var bannerCollecView : ZHBannerCollecView = {
        let banner = ZHBannerCollecView()
        banner.frame = CGRectMake(0, 175, kScreenWidth, 120)
        banner.backgroundColor = UIColor.whiteColor()
        return banner
    }()
    //滚动视图
    lazy var contentTableView : UITableView = {
          let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    lazy var tabView : UIView = {
        let view = UIView()
        view.frame = CGRectMake(0, 0, kScreenWidth, 295)
        return view
    }()
  
}
extension ZHContentVC:UITableViewDataSource,UITableViewDelegate{
   
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if DataArray.count == 0 {
            return 0
        }
        return DataArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellID) as! ZHMainTableViewCell
        cell.dataModel = DataArray[indexPath.row]
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
         tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let model = DataArray[indexPath.row]
        let webVC = ZHMainWebVController()
        webVC.jsurl = model.url
        self.navigationController?.pushViewController(webVC, animated: true)
    }

}
