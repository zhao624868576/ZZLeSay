//
//  ZHMainController.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/7/26.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHMainController: UIViewController {

    var catergoryTitles : [String] = {
       return ["精选", "海淘", "创意生活", "送女票", "科技范", "送爸妈", "送基友", "送闺蜜", "送同事", "送宝贝", "设计感", "文艺范", "奇葩搞怪", "萌萌哒"]
    }()
    
    var categoryView = [UIView]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CardIOUtilities.preload()
       self.setUI()
        
     
    }
       override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setFrame()
    }
    private func setUI(){
        self.view.backgroundColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(gifTarget: self, seletor: #selector(ZHMainController.gifBtnAction))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(searchTarget:self,seletor:#selector(ZHMainController.searchBtnAction))
        self.navigationItem.titleView = titleImageView
        view.addSubview(scrollView)
        view.addSubview(popoverView)
        popoverView.catergoryTitles = catergoryTitles
        for i in 0..<catergoryTitles.count {
            let vc = i == 0 ? ZHContentVC() : ZHContentTableVC()
            addChildViewController(vc)
            view.addSubview(vc.view)
            categoryView.append(vc.view)
        }
    }
    private func setFrame(){
        popoverView.frame = CGRectMake(0, 0, kScreenWidth, 44)
        scrollView.frame = CGRectMake(0, CGRectGetMaxY(popoverView.frame), kScreenWidth, kScreenHeight - CGRectGetMaxY(popoverView.frame) - 49)
        for i in 0..<categoryView.count {
            let view = categoryView[i]
            view.frame = CGRectMake(CGFloat(i) * scrollView.frame.size.width, 0, scrollView.frame.size.width, scrollView.frame.size.height)
            view.backgroundColor = UIColor.init(red:CGFloat(arc4random_uniform(255))/255.0, green: CGFloat(arc4random_uniform(255))/255.0, blue: CGFloat(arc4random_uniform(255))/255.0, alpha: 1)
            scrollView.addSubview(view)
        }
        scrollView.contentSize = CGSizeMake(CGFloat(categoryView.count) * kScreenWidth, 0)
        
    }
    @objc private func gifBtnAction(){
        let loginVc = ZHLoginController()
        self.presentViewController(loginVc, animated: true) {
            
        }
    }
    //开启二维码扫描
    @objc private func searchBtnAction(){
        //触发二维码扫描
        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
        cardIOVC.modalPresentationStyle = .FormSheet
        presentViewController(cardIOVC, animated: true, completion: nil)
    }
    //懒加载
    lazy var titleImageView : UIImageView = {
         let image = UIImage.init(named: "logo")
         let titleView = UIImageView.init(image: image)
         titleView.contentMode = UIViewContentMode.ScaleAspectFit
         titleView.bounds = CGRectMake(0, 0, 20.0 * (image!.size.width / image!.size.height), 20)
         return titleView
    }()
    lazy var scrollView : UIScrollView = {
         let view = UIScrollView()
         view.delegate = self
         view.pagingEnabled = true
         return view
    }()
    lazy var popoverView : ZHPopoverCategoryView = {
          let view = ZHPopoverCategoryView.createPopverCategoryView()
          view.delegate = self
          return view
    }()
   
}
//代理
extension ZHMainController : UIScrollViewDelegate{
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x / kScreenWidth)
        let button = popoverView.cacheScrollCatergoryBtn[index]
        popoverView.clickScrollBtn(button)
    }
}
extension ZHMainController : popverCategoryViewDelegate,CardIOPaymentViewControllerDelegate{
    func selectedCategoryEndWithIndex(index: NSInteger) {
        scrollView.setContentOffset(CGPointMake(scrollView.frame.size.width * CGFloat(index), 0), animated:true)
    }
             //******扫描信用卡代理方法******//
    /// This method will be called if the user cancels the scan. You MUST dismiss paymentViewController.
    func userDidCancelPaymentViewController(paymentViewController: CardIOPaymentViewController!) {
        paymentViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /// This method will be called when there is a successful scan (or manual entry). You MUST dismiss paymentViewController.
    /// @param cardInfo The results of the scan.
    func userDidProvideCreditCardInfo(cardInfo: CardIOCreditCardInfo!, inPaymentViewController paymentViewController: CardIOPaymentViewController!) {
        if let info = cardInfo {
            let str = NSString(format: "Received card info.\\\\n Number: %@\\\\n expiry: %02lu/%lu\\\\n cvv: %@.", info.redactedCardNumber, info.expiryMonth, info.expiryYear, info.cvv)
            print(str)
        }
        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
}