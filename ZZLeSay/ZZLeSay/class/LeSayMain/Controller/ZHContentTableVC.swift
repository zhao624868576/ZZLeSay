//
//  ZHContentTableVC.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/8.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHContentTableVC: UITableViewController {

     private let cellID = "mainCell"
    override func viewDidLoad() {
        super.viewDidLoad()
     tableView.registerNib(UINib.init(nibName: "ZHMainTableViewCell", bundle: nil), forCellReuseIdentifier: cellID)
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellID) as! ZHMainTableViewCell
        cell.goodsImage.image = UIImage.init(named: "strategy_\(Int(arc4random() % 17) + 1).jpg")
        return cell
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 245
    }


}
