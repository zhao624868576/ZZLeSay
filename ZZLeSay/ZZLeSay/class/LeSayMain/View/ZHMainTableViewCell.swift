//
//  ZHMainTableViewCell.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/8.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHMainTableViewCell: UITableViewCell {

    var dataModel : ZHMainBottomModel?{
        didSet{
          title.setTitle(dataModel?.columnModel?.category, forState: .Normal)
          storeName.text = dataModel?.columnModel?.title
          userName.text = dataModel?.autherModel?.nickname
          userIcon.sd_setImageWithURL(dataModel?.autherModel?.avatar_url)
          goodsImage.sd_setImageWithURL(dataModel?.cover_image_url)
          versionNum.text = dataModel?.title
            
            let formatter = NSNumberFormatter.init()
            let str = formatter.stringFromNumber((dataModel?.likes_count)!)
            likeNum .setTitle(str, forState: .Normal)
            likeNum.setTitle(str, forState: .Selected)
        
        }
    }
    
    @IBOutlet weak var title: UIButton!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var goodsImage: UIImageView!
    @IBOutlet weak var versionNum: UILabel!
    @IBOutlet weak var likeNum: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userIcon.layer.borderWidth = 3
        userIcon.layer.borderColor = UIColor.redColor().CGColor
        userIcon.layer.cornerRadius = userIcon.frame.size.width * 0.5
        userIcon.layer.masksToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clickLikeBtn(sender: UIButton) {
        
    }

    
}
