//
//  ZHBannerCollecView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/8.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHBannerCollecView: UICollectionView {

    var dataArray = [ZHMainCenterModel]() {
        didSet{
            self.reloadData()
        }
    }
    //展示滚动图片的数量
    private let cellID : String = "banner"
    init () {
        super.init(frame: CGRectZero, collectionViewLayout: ZHBannerCollectionViewLayout())
        self.delegate = self
        self.dataSource = self
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false

        self.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: cellID)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension ZHBannerCollecView:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.dataArray.count == 0 {
            return 0
        }
        return self.dataArray.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellID, forIndexPath: indexPath)
        let model = self.dataArray[indexPath.row]
        let imageV = UIImageView(frame:cell.contentView.bounds)
        imageV.sd_setImageWithURL(model.image_url)
        cell.contentView.addSubview(imageV)
        return cell
    }
    
}

class ZHBannerCollectionViewLayout: UICollectionViewFlowLayout {
    override func prepareLayout() {
        
        self.itemSize = CGSizeMake(100, 100);
    
        self.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10)
        // 水平滚动
        self.scrollDirection = UICollectionViewScrollDirection.Horizontal
    }
    
}