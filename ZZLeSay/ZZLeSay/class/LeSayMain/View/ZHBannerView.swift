//
//  ZHBannerView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/8.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHBannerView: UIScrollView {
   
    //展示滚动图片的数量
    
    var dataArray = [ZHMainBannerViewModel]() {
        didSet{
           setUI(dataArray)
        }
    }
    init () {
        super.init(frame: CGRectZero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        pageView.frame = CGRectMake(0,160, kScreenWidth, 10)
    }
    //添加子控件
    private func setUI(models : [ZHMainBannerViewModel]?){
       addSubview(scrollView)
        for  i  in 0 ..< dataArray.count {
            let model = models![i]
            let imageView = UIImageView()
            imageView.frame = CGRectMake(kScreenWidth * CGFloat(i), 0, kScreenWidth, 175)
            imageView.tag = i
            imageView.sd_setImageWithURL(model.image_url!)
            scrollView.addSubview(imageView)
        }
        scrollView.contentSize = CGSizeMake(kScreenWidth * CGFloat(dataArray.count), 0)
        addSubview(pageView)
        pageView.currentPage = 0
        pageView.numberOfPages = dataArray.count
        
    }

    //滚动视图
    private lazy var scrollView : UIScrollView = {
        let scorll = UIScrollView()
         scorll.frame = CGRectMake(0, 0, kScreenWidth, 175)
         scorll.showsHorizontalScrollIndicator = false
         scorll.pagingEnabled = true
         scorll.delegate = self
        return scorll
    }()
    //page指示
    private lazy var pageView : UIPageControl = {
        let pages = UIPageControl()
        pages.currentPageIndicatorTintColor = UIColor.cyanColor()
        pages.pageIndicatorTintColor = UIColor.purpleColor()
        return pages
    }()
}
extension ZHBannerView:UIScrollViewDelegate{
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / kScreenWidth
        pageView.currentPage = Int(page)
    }
}
