//
//  ZHHotModel.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/1.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHHotModel: NSObject {

//    "cover_image_url": "http://img01.liwushuo.com/image/160831/3g78uzacx_w.jpg-w720",  //显示图片
//    "cover_webp_url": "http://img01.liwushuo.com/image/160831/3g78uzacx_w.jpg?imageView2/2/w/720/q/85/format/webp",
//    "description": "kinbor Hello Kitty公主梦手帐套装文具礼盒套装14件套", //商品描述
//    "favorites_count": 1596,                            //喜欢数量
//    "is_favorite": false,                               //是否喜欢
//    "price": "119.00",,                                 //价格
//    "purchase_url": "http://s.click.taobao.com/t?sche=liwushuo&e=m%3D2%26s%3DkJWaqBtUl7gcQipKwQzePOeEDrYVVa64yK8Cckff7TVRAdhuF14FMRlSR7m4He4Hxq3IhSJN6GStgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzQIomwaXGXUs78FqzS29vh5nPjZ5WWqolN%2FWWjML5JdM90WyHry0om0UX2TGQsV4H%2FxgcGVwrnwQs6ndezR%2F2lgHgX7hGJugLw%3D%3D&lws=1",   //个人网店...淘宝店平台
//    "url": "http://www.liwushuo.com/items/1068152",     //个人网店...礼物说平台
    
    var cover_image_url : NSURL?  //图像地址
//    var cover_webp_url : NSURL?
    var name : String?    //名称
    
    var favorites_count :NSNumber? //喜欢数
    
    var is_favorite : Bool = false //登录账户是否喜欢
    var price : String?         //价格
    var purchase_url : NSURL?
    var url :String?
    
    
    // MARK:- 自定义构造函数
    init(dict : [String : AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {}
}
