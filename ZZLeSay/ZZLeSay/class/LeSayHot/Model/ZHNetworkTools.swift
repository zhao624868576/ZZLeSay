//
//  ZHNetworkTools.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/2.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import AFNetworking


// 定义枚举类型
enum RequestType : String {
    case GET = "GET"
    case POST = "POST"
}
enum GiftType : String {
    case One = "One"      //每日推荐
    case Two = "Two"      //TOP100
    case Three = "Three"  //原创榜
    case Four = "Four"    //新星榜
}
class ZHNetworkTools: AFHTTPSessionManager {
    // let是线程安全的
    static let shareInstance : ZHNetworkTools = {
        let tools = ZHNetworkTools()
        tools.responseSerializer.acceptableContentTypes?.insert("text/html")
        tools.responseSerializer.acceptableContentTypes?.insert("text/plain")
        
        return tools
    }()
}
// MARK:- 封装请求方法
extension ZHNetworkTools {
    func request(methodType : RequestType, urlString : String, parameters : [String : AnyObject]?, finished : (result : AnyObject?, error : NSError?) -> ()) {
        
        // 1.定义成功的回调闭包
        let successCallBack = { (task : NSURLSessionDataTask, result : AnyObject?) -> Void in
            finished(result: result, error: nil)
        }
        
        // 2.定义失败的回调闭包
        let failureCallBack = { (task : NSURLSessionDataTask?, error : NSError) -> Void in
            finished(result: nil, error: error)
        }
        
        // 3.发送网络请求
        if methodType == .GET {
            GET(urlString, parameters: parameters, progress: nil, success: successCallBack, failure: failureCallBack)
        } else {
            POST(urlString, parameters: parameters, progress: nil, success: successCallBack, failure: failureCallBack)
        }
    }
}
// MARK:- 请求热点首页数据
extension ZHNetworkTools {

    func loadHotNetworkData(finshed:((result : [[String : AnyObject]]?,error : NSError?)->())) {
        // 1.获取请求的URLString
        let urlString = "http://api.liwushuo.com/v2/items?gender=1&generation=1&limit=50&offset=0"
        request(.GET, urlString: urlString, parameters: nil) { (result, error) in
            guard let user = result as?[String:AnyObject] else{
               finshed(result: nil, error: error)
                return
            }
            let userDict = user["data"] as? [String : AnyObject]
            finshed(result: userDict!["items"] as? [[String : AnyObject]], error: error)
        }
    }
}
// MARK:- 请求分类单品数据
extension ZHNetworkTools {
    
    func loadCategoryItemData(finshed:((result : [[String : AnyObject]]?,error : NSError?)->())){
        // 1.获取请求的URLString
        let urlString = "http://api.liwushuo.com/v2/item_categories/tree?"
        request(.GET, urlString: urlString, parameters: nil) { (result, error) in
            guard let user = result as?[String:AnyObject] else{
                finshed(result: nil, error: error)
                return
            }
            let userDict = user["data"] as? [String : AnyObject]
            finshed(result: userDict!["categories"] as? [[String : AnyObject]], error: error)
        }
    }
}
// MARK:- 请求首页中间滚动图片
extension ZHNetworkTools{
    func loadMainCenterContentData(finshed:((result : [[String : AnyObject]]?,error : NSError?)->())) {
        // 1.获取请求的URLString
        let urlString = "http://api.liwushuo.com/v2/secondary_banners?gender=1&generation=1"
        request(.GET, urlString: urlString, parameters: nil) { (result, error) in
            guard let user = result as?[String:AnyObject] else{
                finshed(result: nil, error: error)
                return
            }
            let userDict = user["data"] as? [String : AnyObject]
            finshed(result: userDict!["secondary_banners"] as? [[String : AnyObject]], error: error)
        }
    }
}
// MARK:- 请求首页顶部广告位数据,滚动视图
extension ZHNetworkTools{
    func loadBannerViewData(finshed:((result : [[String : AnyObject]]?,error : NSError?)->())) {
        // 1.获取请求的URLString
        let urlString = "http://api.liwushuo.com/v2/banners?channel=iOS"
        request(.GET, urlString: urlString, parameters: nil) { (result, error) in
            guard let user = result as?[String:AnyObject] else{
                finshed(result: nil, error: error)
                return
            }
            let userDict = user["data"] as? [String : AnyObject]
            finshed(result: userDict!["banners"] as? [[String : AnyObject]], error: error)
        }
    }
}
// MARK:- 请求首页下部分商家主要内容展示
extension ZHNetworkTools {
    func loadMainContentData(finshed:((result : [[String : AnyObject]]?,error : NSError?)->())) {
        // 1.获取请求的URLString
        let urlString = "http://api.liwushuo.com/v2/channels/100/items_v2?ad=2&gender=1&generation=1&limit=20&offset=0"
        request(.GET, urlString: urlString, parameters: nil) { (result, error) in
            guard let user = result as?[String:AnyObject] else{
                finshed(result: nil, error: error)
                return
            }
            let userDict = user["data"] as? [String : AnyObject]
            finshed(result: userDict!["items"] as? [[String : AnyObject]], error: error)
        }
    }
}
// MARK:- 请求选礼神器数据
extension ZHNetworkTools{
    func loadGiftAllData(index:GiftType,finshed:((result : [String : AnyObject]?,error : NSError?)->())) {
        // 1.获取请求的URLString
        var urlString = ""
        switch index {
        case GiftType.One:
            urlString = "http://api.liwushuo.com/v2/ranks_v2/ranks/1?limit=20&offset=0"
            break
        case GiftType.Two:
            urlString = "http://api.liwushuo.com/v2/ranks_v2/ranks/2?limit=20&offset=0"
            break
        case GiftType.Three:
            urlString = "http://api.liwushuo.com/v2/ranks_v2/ranks/3?limit=20&offset=0"
            break
        case GiftType.Four:
            urlString = "http://api.liwushuo.com/v2/ranks_v2/ranks/4?limit=20&offset=0"
            break
        }
        request(.GET, urlString: urlString, parameters: nil) { (result, error) in
            guard let user = result as?[String:AnyObject] else{
                finshed(result: nil, error: error)
                return
            }
            let userDict = user["data"] as? [String : AnyObject]
            finshed(result: userDict, error: error)
        }
    }
}

