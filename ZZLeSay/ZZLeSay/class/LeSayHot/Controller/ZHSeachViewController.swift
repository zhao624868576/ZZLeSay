//
//  ZHSeachViewController.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/9.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHSeachViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.init(red: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1)
        navigationItem.titleView = search
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: backButton)
        view.addSubview(contenTableView)
        contenTableView.autoresizesSubviews = false
    }
    //顶部search框
    private lazy var search : UISearchBar = {
        let sh = UISearchBar()
        sh.delegate = self
        sh.searchBarStyle = .Default
        sh.barTintColor = UIColor.redColor()
        sh.placeholder = "快选一份礼物,送给亲爱的Ta吧"
        return sh
    }()
    lazy var backButton : UIButton = {
        let btn = UIButton()
        btn.titleLabel?.font = UIFont.systemFontOfSize(14)
        btn.setTitle("取消", forState: .Normal)
        btn.sizeToFit()
        btn.addTarget(self, action: #selector(self.backAction), forControlEvents: .TouchUpInside)
        return btn
    }()
    lazy var contenTableView : UITableView = {
          let table = UITableView(frame: CGRectMake(0, 0, kScreenWidth, kScreenHeight), style: .Grouped)
          table.delegate = self
          table.dataSource = self
          table . sectionFooterHeight = 1.0
          return table
    }()
    lazy var markCollection : ZHMarkCollectionView = {
        let collection = ZHMarkCollectionView(frame: CGRectZero, collectionViewLayout: UICollectionViewFlowLayout())

        return collection
    }()

}
extension ZHSeachViewController:UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let searchID = "searchID"
        var cell = tableView.dequeueReusableCellWithIdentifier(searchID)
        if  cell == nil {
            cell = UITableViewCell.init(style: .Default, reuseIdentifier: searchID)
        }
        if indexPath.section == 0 {
            markCollection.frame = CGRectMake(0, 0, kScreenWidth, 120)
            cell?.contentView.addSubview(markCollection)
        }else {
            cell?.imageView?.image = UIImage.init(named: "giftcategory_guide")
            cell?.textLabel?.text = "使用选礼神器快速挑选礼物"
            cell?.textLabel?.font = UIFont.systemFontOfSize(14)
            cell?.accessoryType = .DisclosureIndicator
        }
        cell?.selectionStyle = .None
        return cell!
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
        return "大家都在搜"
        }
        return nil
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 10
        }
        return 35
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 120
        }
        return 49
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 1{
            self.navigationController?.pushViewController(ZHSelectGiftVC(), animated: true)
        }
    }
    @objc func backAction(){
        print("123456")
    }
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        print("searchBarTextDid")
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        search.resignFirstResponder()
    }
}



