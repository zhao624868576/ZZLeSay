//
//  ZHMarkViewCell.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/9.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHMarkViewCell: UICollectionViewCell {

    // 标签名称 属性
   var keyWord: String? {
        didSet{
            markBtn.setTitle(keyWord, forState: .Normal)
            layoutIfNeeded()
            updateConstraintsIfNeeded()
        }
    }
    @IBOutlet weak var markBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        markBtn.layer.borderColor = UIColor.init(red: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1).CGColor
        markBtn.layer.borderWidth = 1
        markBtn.layer.cornerRadius = 4
        markBtn.layer.masksToBounds = true
    }
    @IBAction func clickMarkBtn(sender: UIButton) {
        
    }
    func sizeForCell() ->CGSize {
        let btnSize : CGSize = (self.markBtn.titleLabel?.sizeThatFits(CGSizeMake(CGFloat(MAXFLOAT), CGFloat(MAXFLOAT))))!
        return CGSizeMake(btnSize.width + 30, 30)
    }

}
