//
//  ZHMarkCollectionView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/9.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHMarkCollectionView: UICollectionView {
    
    let markCellID = "markCellID"
    let data = ["零食","手机壳","历史记录","测试","妈妈说标题要长","机智的手哥","测试数据","云标签","连衣裙","情侣装"]
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        registerNib(UINib.init(nibName: "ZHMarkViewCell", bundle: nil), forCellWithReuseIdentifier: markCellID)
        delegate = self
        dataSource = self
        backgroundColor = UIColor.whiteColor()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension ZHMarkCollectionView:UICollectionViewDataSource,UICollectionViewDelegate{
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(markCellID, forIndexPath: indexPath) as!ZHMarkViewCell
        cell.keyWord = data[indexPath.row]
        return cell
    }
    func collectionView(collectionView:UICollectionView,layout collectionViewLayout:UICollectionViewLayout, sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize {
          let cell = NSBundle.mainBundle().loadNibNamed("ZHMarkViewCell", owner: nil, options: nil).first as! ZHMarkViewCell
          cell.keyWord = data[indexPath.row];
        return cell.sizeForCell()
    }
   
}