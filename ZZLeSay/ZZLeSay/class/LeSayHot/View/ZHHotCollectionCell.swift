//
//  ZHHotCollectionCell.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/8.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit
import SDWebImage

class ZHHotCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var descripLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    
    var selectBtn : Bool = true
    
    var hotModel : ZHHotModel?{
        didSet{
           
            //nil校验
            guard let hotModel = hotModel else{
               return
            }
            //设置图片
            self.imageV.sd_setImageWithURL(hotModel.cover_image_url, placeholderImage: nil)
            //设置描述文字
            self.descripLabel.text = hotModel.name
            //设置价格
            self.priceLabel.text = "¥ " + hotModel.price!
            //设置喜欢数
            
            let formatter = NSNumberFormatter.init()
            let str = formatter.stringFromNumber(hotModel.favorites_count!)
            
            self.likeButton .setTitle(str, forState: .Normal)
            self.likeButton.setTitle(str, forState: .Selected)
            
//            self.likeButton.titleLabel?.text = "\(hotModel.favorites_count)"
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    @IBAction func clickLikeBtn(sender: UIButton) {
        
        sender.selected = selectBtn
        selectBtn = !sender.selected
        
    }
}
