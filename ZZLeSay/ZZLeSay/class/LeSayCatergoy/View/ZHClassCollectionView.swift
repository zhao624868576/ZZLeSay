//
//  ZHClassCollectionView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/4.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHClassCollectionView: UICollectionView,UICollectionViewDelegate,UICollectionViewDataSource{
  
    private let cellID : String = "collectionCell"
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.delegate = self
        self.dataSource = self
        self.showsHorizontalScrollIndicator = false
        self.registerNib(UINib.init(nibName: "ZHClassifyCollectionCell", bundle: nil), forCellWithReuseIdentifier: cellID)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension ZHClassCollectionView{
   
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 16
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellID, forIndexPath: indexPath)as! ZHClassifyCollectionCell
        if indexPath.row == 15 {
            cell.imageV.hidden = true
            cell.bigTitle.hidden = true
            cell.updateNumLabel.hidden = true
            cell.authorNameLabel.hidden = true
            cell.lookAllBtn.hidden = false
        }else{
            cell.imageV.hidden = false
            cell.bigTitle.hidden = false
            cell.updateNumLabel.hidden = false
            cell.authorNameLabel.hidden = false
            cell.lookAllBtn.hidden = true
            cell.imageV.image = UIImage.init(named: "strategy_\(Int(arc4random() % 17) + 1).jpg")
        }
        return cell
    }

}
