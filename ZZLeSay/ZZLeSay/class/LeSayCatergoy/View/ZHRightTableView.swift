//
//  ZHRightTableView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/10.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHRightTableView: UICollectionView {
    
    var rightArray = [ZHSubItemModel]()  {
        didSet{
            self.reloadData()
        }
    }
    var rightDataArray = [ZHItemModel]() {
        didSet{
            self.reloadData()
        }
    }
    init(frame: CGRect) {
        let laout = ZHRightCollViewLaout()
        super.init(frame: frame, collectionViewLayout: laout)
        self.dataSource = self
        self.delegate = self
        self.backgroundColor = UIColor.whiteColor()

        self.registerNib(UINib.init(nibName: "ZHRightCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "rightCell")

        self.registerNib(UINib.init(nibName: "ZHRightReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "rightHeaderID")
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
extension ZHRightTableView:UICollectionViewDelegate,UICollectionViewDataSource {
   
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        if self.rightDataArray.count == 0 {
            return 0
        }
        return self.rightDataArray.count
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let model = self.rightDataArray[section]
        if model.subcategories?.count == 0 {
            return 0
        }
        return model.tempArray.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCellWithReuseIdentifier("rightCell", forIndexPath: indexPath) as! ZHRightCollectionViewCell
        let model = self.rightDataArray[indexPath.section]
        let subModel = model.tempArray[indexPath.row]
        cell.itemModel = subModel
        return cell
    }

    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier:"rightHeaderID", forIndexPath: indexPath) as! ZHRightReusableView
        let model = self.rightDataArray[indexPath.section]
        header.itemMode = model
        return header
    }
}
class ZHRightCollViewLaout: UICollectionViewFlowLayout {
 
    override func prepareLayout() {
        self.itemSize = CGSizeMake(kScreenWidth / 4, kScreenWidth / 4 + 25)
        self.scrollDirection = UICollectionViewScrollDirection.Vertical
        self.minimumLineSpacing = 5
        self.minimumInteritemSpacing = 0
        self.headerReferenceSize = CGSizeMake(kScreenWidth * 0.75, 70);

    }
}