//
//  ZHClassifyTitleView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/3.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit


protocol classifyTitleDelegate {
    func selectedIndex(index : Int)
}
class ZHClassifyTitleView: UIView {
    
    var delegate : classifyTitleDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setUI()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setUIFrame()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func setUI(){
        addSubview(stratery)
        addSubview(product)
        addSubview(tipView)
        
        stratery.addTarget(self, action: #selector(self.clickStratery(_:)), forControlEvents: .TouchUpInside)
        product.addTarget(self, action: #selector(self.clickProduct(_:)), forControlEvents:.TouchUpInside)
    }
    private func setUIFrame(){
        stratery.frame = CGRectMake(0, 0, self.frame.size.width * 0.5, self.frame.size.height)
        product.frame = CGRectMake(self.frame.size.width * 0.5, 0, self.frame.size.width * 0.5, self.frame.size.height)
        tipView.frame = CGRectMake(0, self.frame.size.height - 2, self.frame.size.width * 0.5, 2)
    }
    lazy var stratery : UIButton = {
        let button = UIButton()
        button.tag = 0
        button.setTitle("攻略", forState: .Normal)
        button.titleLabel?.font = UIFont.systemFontOfSize(15)
        button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        return button
    }()
    lazy var product : UIButton = {
        let button = UIButton()
        button.tag = 1
        button.setTitle("单品", forState: .Normal)
        button.titleLabel?.font = UIFont.systemFontOfSize(15)
        button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        return button
    }()
    lazy var tipView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.whiteColor()
        return view
    }()
}
extension ZHClassifyTitleView{
    @objc func clickStratery(button : UIButton){
         UIView.animateWithDuration(0.5, animations: {
     self.tipView.frame = CGRectMake(0, self.frame.size.height - 2, self.frame.size.width * 0.5, 2)
            }) { (_) in
           self.delegate?.selectedIndex(button.tag)
        }
    }
    @objc func clickProduct(button : UIButton){
        UIView.animateWithDuration(0.5, animations: {
            self.tipView.frame = CGRectMake(self.frame.size.width * 0.5, self.frame.size.height - 2, self.frame.size.width * 0.5, 2)
        }) { (_) in
           self.delegate?.selectedIndex(button.tag)
        }
    }
}
