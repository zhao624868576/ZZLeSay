//
//  ZHRightReusableView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/8.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHRightReusableView: UICollectionReusableView {

    @IBOutlet weak var categoryLabel: UILabel!
    
    var itemMode : ZHItemModel?{
        didSet{
            categoryLabel.text = itemMode?.name
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
