//
//  ZHClassifyCollectionCell.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/4.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHClassifyCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var bigTitle: UILabel!
    @IBOutlet weak var updateNumLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var lookAllBtn: UIButton!
    
    var indexBlock : ((sender: UIButton)->())?
    
    @IBAction func clickLookAllBtn(sender: UIButton) {
        print("点击查看全部")
        indexBlock!(sender: sender)
    }
    override func awakeFromNib() {
        lookAllBtn.layer.borderColor = lookAllBtn.titleLabel?.textColor.CGColor
        lookAllBtn.layer.borderWidth = 1
        lookAllBtn.layer.cornerRadius = 6
        lookAllBtn.layer.masksToBounds = true
    }
}
