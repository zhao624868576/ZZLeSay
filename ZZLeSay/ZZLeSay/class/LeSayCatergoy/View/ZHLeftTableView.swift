//
//  ZHLeftTableView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/10.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHLeftTableView: UITableView,UITableViewDelegate,UITableViewDataSource{
 
    var leftDataArray = [ZHItemModel]() {
        didSet{
            self.reloadData()
        }
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        self.delegate = self
        self.dataSource = self
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        self.separatorStyle = UITableViewCellSeparatorStyle.None
        self.backgroundColor = UIColor(red: 235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 1)
        self.registerClass(UITableViewCell.self, forCellReuseIdentifier: "leftCell")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
extension ZHLeftTableView{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.leftDataArray.count == 0 {
            return 0
        }
       return self.leftDataArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let ID = "leftCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(ID)
        let backView = UIView.init(frame: (cell?.contentView.bounds)!)
        backView.backgroundColor = UIColor.whiteColor()
        cell?.backgroundColor = UIColor(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1)
        cell?.selectedBackgroundView = backView
        let model = self.leftDataArray[indexPath.row]
        cell?.textLabel?.text = model.name
        cell?.textLabel?.font = UIFont.systemFontOfSize(13)
        cell?.textLabel?.textAlignment = NSTextAlignment.Center
        if indexPath.row == 0 {
            cell?.setSelected(true, animated: true)
        }
        return cell!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
}