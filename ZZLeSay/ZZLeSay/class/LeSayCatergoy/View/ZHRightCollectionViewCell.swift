//
//  ZHRightCollectionViewCell.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/8.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHRightCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var titleL: UILabel!
    
    var itemModel : ZHSubItemModel?{
        didSet{
           self.imageV.sd_setImageWithURL(itemModel?.icon_url)
           self.titleL.text = itemModel?.name
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
