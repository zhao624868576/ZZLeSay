//
//  ZHCatergroyController.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/7/26.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit
import MJRefresh

class ZHCatergroyController: UIViewController{

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        //下拉刷新
        oneTabelView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
            self.oneTabelView.mj_header.beginRefreshing()
            dispatch_after(2, dispatch_get_main_queue(), { 
                self.oneTabelView.mj_header.endRefreshing()
            })
        })
    }
    override func viewWillLayoutSubviews() {
       super.viewWillLayoutSubviews()
        titleView.frame = CGRectMake(0, 0, 120.0, 44.0)
        search.frame = CGRectMake(0, 0, kScreenWidth, 50)
        contentScrollView.frame = CGRectMake(0, CGRectGetMaxY(search.frame), kScreenWidth, kScreenHeight - CGRectGetMaxY(search.frame) - 49 - 20)
        oneTabelView.frame = CGRectMake(0, 0, kScreenWidth, contentScrollView.frame.size.height)
        twoUIView.view.frame = CGRectMake(kScreenWidth, 0, kScreenWidth, contentScrollView.frame.size.height)
         classifyTitle.frame = CGRectMake(0, 0, kScreenWidth, 30)
        
    }
    private func setUI(){
      self.automaticallyAdjustsScrollViewInsets = false
      self.view.backgroundColor = UIColor.whiteColor()
      navigationItem.titleView = titleView
      navigationItem.rightBarButtonItem = UIBarButtonItem.init(Target: self, seletor: #selector(self.clickBarButton))
      navigationItem.rightBarButtonItem?.customView?.alpha = 0
        
      self.view.addSubview(search)
      self.view.addSubview(contentScrollView)
        contentScrollView.addSubview(oneTabelView)
        contentScrollView.addSubview(twoUIView.view)
        oneTabelView.tableHeaderView = headerView
        headerView.addSubview(classifyTitle)
        headerView.addSubview(classifyCollection)
    }
    //顶部单选
    private lazy var titleView: ZHClassifyTitleView = {
        let view = ZHClassifyTitleView()
        view.delegate = self
        return view
    }()
    //顶部search框
    private lazy var search : UISearchBar = {
       let sh = UISearchBar()
        sh.delegate = self
        sh.searchBarStyle = .Minimal
        sh.placeholder = "快选一份礼物,送给亲爱的Ta吧"
        return sh
    }()
    //内容控件
    private lazy var contentScrollView : UIScrollView = {
       let scroll = UIScrollView()
        scroll.pagingEnabled = true
        scroll.contentSize = CGSizeMake(2 * kScreenWidth, 0)
        scroll.delegate = self
        return scroll
    }()
    //添加scrollview的子控件
    private lazy var oneTabelView : UITableView = {
        let table = UITableView(frame: CGRectZero, style: .Grouped)
        table.dataSource = self
        table.delegate = self
//        table.sectionFooterHeight = 0
//        table.separatorStyle = .None
        return table
    }()
    private lazy var twoUIView : ZHItemViewController = {
         let view = ZHItemViewController()
         return view
    }()
    //内容头部视图,放置collectionView
    private lazy var headerView : UIView = {
        let header = UIView()
        header.frame = CGRectMake(0, 0, kScreenWidth, 190)
        return header
    }()
    //创建各分类标题
    private var classifyTitle : ZHClassTitle = {
         let title = ZHClassTitle.creatClassifyTitl()
         return title
    }()
    
    
    //创建collectionView
    private lazy var classifyCollection : ZHClassCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSizeMake(200, 65);
        // 设置最小行间距
        flowLayout.minimumLineSpacing = 10;
        // 设置垂直间距
        flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 0)
        flowLayout.minimumInteritemSpacing = 5;
        // 设置滚动方向
        flowLayout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        let collection = ZHClassCollectionView(frame: CGRectMake(0, 30, kScreenWidth, 190 - 30), collectionViewLayout: flowLayout)
        collection.backgroundColor = UIColor.yellowColor()
        return collection
    }()
    
}
extension ZHCatergroyController:UITableViewDelegate,UITableViewDataSource{
   
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let CellID = "cellID"
        let margin : CGFloat = 10
        let buttonW : CGFloat = (kScreenWidth - 3 * margin) * 0.5
        let buttonH : CGFloat = (kScreenWidth - 4 * margin) / 3
        var cell = tableView.dequeueReusableCellWithIdentifier(CellID)
        if  cell == nil {
            cell = UITableViewCell.init(style: .Default, reuseIdentifier: CellID)
        }
        let cellView = UIView()
        cellView.frame = CGRectMake(0, 0, kScreenWidth, kScreenWidth)
        cell?.contentView.addSubview(cellView)
        for  i  in 0 ..< 6  {
            let button = UIButton()
            let buttonX  = i % 2
            let buttonY  = i / 2
            button.tag = i
            button.frame = CGRectMake(CGFloat(buttonX) * (buttonW + margin)+margin, CGFloat(buttonY) * (buttonH + margin)+margin, buttonW, buttonH)
            button.setImage(UIImage.init(named: "strategy_\(Int(arc4random() % 17) + 1).jpg"), forState: .Normal)
            button.addTarget(self, action: #selector(self.clickImageButton(_:)), forControlEvents: .TouchUpInside)
           cellView.addSubview(button)
        }
        cell?.selectionStyle = .None
        return cell!
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let classifyTitles = ZHClassTitle.creatClassifyTitl()
        classifyTitles.frame = CGRectMake(0, 0, kScreenWidth, 35)
        return classifyTitles
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return kScreenWidth
    }
    @objc func clickImageButton(button : UIButton){
      print(button.tag)
    }
    
}
extension ZHCatergroyController:classifyTitleDelegate,UISearchBarDelegate,UIScrollViewDelegate{
    ///点击选礼神器
    @objc func clickBarButton(){
        let giftVC = ZHSelectGiftVC()
        self.navigationController?.pushViewController(giftVC, animated: true)
    }
    func selectedIndex(index : Int){
        if index == 0 {
            navigationItem.rightBarButtonItem?.customView?.alpha = 0
            contentScrollView.contentOffset.x = 0
        }else{
            navigationItem.rightBarButtonItem?.customView?.alpha = 1
            contentScrollView.contentOffset.x = kScreenWidth
        }
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
      
    }
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        print("searchBarTextDid")
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
}
