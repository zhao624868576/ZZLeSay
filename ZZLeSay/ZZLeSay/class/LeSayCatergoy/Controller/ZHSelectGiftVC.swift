//
//  ZHSelectGiftVC.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/9.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHSelectGiftVC: UIViewController {
    
    var tempIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.init(red: 235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 1)
        title = "选礼神器"
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: cancelButton)
        self.view.addSubview(titleView)
        self.view.addSubview(scrollView)
        scrollView.contentSize = CGSizeMake(kScreenWidth * 4, 0)
        addChildCollectionView(0)
    }
       //添加子控件
    func addChildCollectionView(index : Int) {
        switch index {
        case 0:
            self.scrollView.addSubview(dailyRecommandView)
            dailyRecommandView.frame = CGRectMake(CGFloat(index) * kScreenWidth, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height)
            self.scrollView.contentOffset = CGPointMake(CGFloat(index) * kScreenWidth, 0)
            break
        case 1:
            self.scrollView.addSubview(topHundredView)
            topHundredView.frame = CGRectMake(CGFloat(index) * kScreenWidth, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height)
            self.scrollView.contentOffset = CGPointMake(CGFloat(index) * kScreenWidth, 0)
            break
        case 2:
            self.scrollView.addSubview(aloneOriginalView)
            aloneOriginalView.frame = CGRectMake(CGFloat(index) * kScreenWidth, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height)
            self.scrollView.contentOffset = CGPointMake(CGFloat(index) * kScreenWidth, 0)
            break
        case 3:
            self.scrollView.addSubview(newStarView)
            newStarView.frame = CGRectMake(CGFloat(index) * kScreenWidth, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height)
            self.scrollView.contentOffset = CGPointMake(CGFloat(index) * kScreenWidth, 0)
            break
        default:
            break
        }
    }
    lazy var cancelButton : UIButton = {
        let btn = UIButton()
        btn.setBackgroundImage(UIImage.init(named: "icon_sort"), forState: .Normal)
        btn.sizeToFit()
        btn.addTarget(self, action: #selector(self.backAction), forControlEvents: .TouchUpInside)
        return btn
    }()
    lazy var titleView : ZHGiftTitleView = {
        let giftView = ZHGiftTitleView(frame: CGRectMake(0,0,kScreenWidth,35))
        giftView.indexBlock = {(index : Int)-> Void in
           self.tempIndex = index
           self.addChildCollectionView(index)
        }
        giftView.backgroundColor = UIColor.whiteColor()
        
        return giftView
    }()
    lazy var scrollView : UIScrollView = {
        let v = UIScrollView(frame: CGRectMake(0,CGRectGetMaxY(self.titleView.frame),kScreenWidth,kScreenHeight - CGRectGetMaxY(self.titleView.frame) - 64))
        v.backgroundColor = UIColor.whiteColor()
        v.showsHorizontalScrollIndicator = false
        v.showsVerticalScrollIndicator = false
        v.pagingEnabled = true
        v.delegate = self
        return v
    }()
    //每日推荐
    lazy var dailyRecommandView : ZHDailyRecommandView = {
           let dailyRecommand = ZHDailyRecommandView()
        dailyRecommand.backgroundColor = UIColor(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1)
        return dailyRecommand
    }()
    //TOP100
    lazy var topHundredView : ZHTopHundredView = {
        let topHundred = ZHTopHundredView()
        topHundred.backgroundColor = UIColor(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1)
        return topHundred
    }()
    //独立原创榜
    lazy var aloneOriginalView : ZHAloneOriginalView = {
        let aloneOriginal = ZHAloneOriginalView()
        aloneOriginal.backgroundColor = UIColor(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1)
        return aloneOriginal
    }()
    //新星榜
    lazy var newStarView : ZHNewStarView = {
        let newStar = ZHNewStarView()
        newStar.backgroundColor = UIColor(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1)
        return newStar
    }()
    deinit{
     NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}
extension ZHSelectGiftVC:UIScrollViewDelegate{
    @objc func backAction(){
        print("123456")
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
      
        let offset : Int = Int(scrollView.contentOffset.x) / Int(kScreenWidth)
        self.addChildCollectionView(offset)
        NSNotificationCenter.defaultCenter().postNotificationName("kscrollOffsetIndexNotification", object: nil, userInfo: ["index":offset])
    }

}
class ZHGiftTitleView : UIView {
   
    let titleArray = ["每日推荐","TOP100","独立原创榜","新星榜"]
    lazy var tipView : UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.redColor()
        return v
    }()
    
   var tempBtn = UIButton()
   var indexBlock : ((index : Int)->())?

   override init(frame: CGRect) {
        super.init(frame: frame)
    NSNotificationCenter.defaultCenter().addObserverForName("kscrollOffsetIndexNotification", object: nil, queue: NSOperationQueue.mainQueue()) { (notice : NSNotification) in
        let index = notice.userInfo!["index"] as? Int
        for button in self.subviews{
            if button.isKindOfClass(UIButton){
                guard let btn = button as? UIButton else{return}
                if index == btn.tag{
                self.clickTitleBtn(btn)
                }
            }
        }
    }
    for i in 0..<titleArray.count {
        let btn = UIButton(type: .Custom)
        btn.frame = CGRectMake(frame.size.width / CGFloat(titleArray.count)  * CGFloat(i), 0, frame.size.width / CGFloat(titleArray.count), frame.size.height)
        btn.tag = i
        btn.setTitle(titleArray[i], forState: UIControlState.Normal)
        btn.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        btn.setTitleColor(UIColor.redColor(), forState: UIControlState.Selected)
        btn.titleLabel?.font = UIFont.systemFontOfSize(12)
        btn.addTarget(self, action: #selector(self.clickTitleBtn(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(btn)
        if btn.tag == 0 {
            self.addSubview(tipView)
            self.clickTitleBtn(btn)
            tipView.frame = CGRectMake(0, frame.size.height - 2, frame.size.width / CGFloat(titleArray.count), 2)
            UIView.animateWithDuration(0.25, animations: {
                let text  = self.titleArray[i]
                let width = text.boundingRectWithSize(CGSizeZero, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(12)], context:nil).size.width
                self.tipView.frame.size.width = width
                self.tipView.center.x = btn.center.x
            })
        }
    }
}
    @objc func clickTitleBtn(button:UIButton){
        if button.selected {
           return
        }
         button.selected = true
         tempBtn.selected = false
         tempBtn = button
        if indexBlock != nil {
            indexBlock!(index: button.tag)
        }
        UIView.animateWithDuration(0.25, animations: {
            self.tipView.frame.size.width = (button.titleLabel?.frame.size.width)!
            self.tipView.center.x = button.center.x
            }) { (_) in
                
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
  }




