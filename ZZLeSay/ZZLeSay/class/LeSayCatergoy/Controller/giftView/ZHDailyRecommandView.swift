//
//  ZHDailyRecommandView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/9.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit
import MJRefresh

class ZHDailyRecommandView: UICollectionView {

    var topImage : String? {
        didSet{
          tempResuaView!.ImageUrl = topImage
        }
    }
    var dataDailyArray = [ZHDailyRecommandModel]() {
        didSet{
           self.reloadData()
        }
    }
    var tempResuaView : ZHDailyRecommandReusableView?
    
    init(){
     super.init(frame: CGRectZero, collectionViewLayout: ZHDailyRecommandLayout())
        self.dataSource = self
        self.delegate = self
        
        self.registerNib(UINib.init(nibName: "ZHGiftCollectionCell", bundle: nil), forCellWithReuseIdentifier: "dailyRecommand")
        self.registerNib(UINib.init(nibName: "ZHDailyRecommandReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "dailRecommandHeader")
        
        self.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
            self.loadNetwork()
        })
        self.mj_header.beginRefreshing()
    }
    //网络请求
    private func loadNetwork() {
        ZHNetworkTools.shareInstance.loadGiftAllData(GiftType.One) { (result, error) in
            
            self.mj_header.endRefreshing()
            
            if error != nil{return}
            let imgeUrl = result!["cover_image"] as? String
            self.topImage = imgeUrl
//            self.topImage = (result!["cover_imge"] as? String)!
            guard let items = result!["items"] as?[[String:AnyObject]] else{
                return
            }
            for dict in items{
                let model = ZHDailyRecommandModel(dict: dict)
                self.dataDailyArray.append(model)
            }
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
extension ZHDailyRecommandView:UICollectionViewDataSource,UICollectionViewDelegate{


    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataDailyArray.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("dailyRecommand", forIndexPath: indexPath) as! ZHGiftCollectionCell
        cell.DailyModel = dataDailyArray[indexPath.row]
        return cell
    }
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let coll = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "dailRecommandHeader", forIndexPath: indexPath) as! ZHDailyRecommandReusableView
        self.tempResuaView = coll
        return coll
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let model = dataDailyArray[indexPath.row]
        let webView = ZHMainWebVController()
        webView.jsurl = model.url
        
        
    }
}
class ZHDailyRecommandLayout: UICollectionViewFlowLayout {
    override func prepareLayout() {
        itemSize = CGSizeMake(kScreenWidth / 2 - 10, kScreenWidth / 2 * 1.35)
        scrollDirection = UICollectionViewScrollDirection.Vertical
        minimumLineSpacing = 10
        minimumInteritemSpacing = 0
        sectionInset = UIEdgeInsetsMake(5, 5, 0, 5)
        headerReferenceSize = CGSizeMake(kScreenWidth, 150);
    }
}