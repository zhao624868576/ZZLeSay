//
//  ZHDailyRecommandReusableView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/13.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHDailyRecommandReusableView: UICollectionReusableView {

    var ImageUrl : String? {
        didSet{
          imageV.sd_setImageWithURL(NSURL.init(string: ImageUrl!))
        }
    }
    
    @IBOutlet weak var imageV: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    
}
