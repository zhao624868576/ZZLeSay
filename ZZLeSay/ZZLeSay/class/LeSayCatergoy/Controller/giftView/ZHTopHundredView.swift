//
//  ZHTopHundredView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/9.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit
import MJRefresh

class ZHTopHundredView: UICollectionView {
    
    var dataTopArray = [ZHTopHunderModel]() {
        didSet{
            self.reloadData()
        }
    }

    init(){
        super.init(frame: CGRectZero, collectionViewLayout: ZHTopHundredLayout())
        self.dataSource = self
        self.delegate = self
        
        self.registerNib(UINib.init(nibName: "ZHGiftCollectionCell", bundle: nil), forCellWithReuseIdentifier: "dailyRecommand")
        self.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
            self.loadNetwork()
        })
        self.mj_header.beginRefreshing()
    }
    //网络请求
    private func loadNetwork() {
        
        ZHNetworkTools.shareInstance.loadGiftAllData(GiftType.Two) { (result, error) in
             self.mj_header.endRefreshing()
            if error != nil{return}
            guard let items = result!["items"] as?[[String:AnyObject]] else{
                return
            }
            for dict in items{
                let model = ZHTopHunderModel(dict: dict)
                self.dataTopArray.append(model)
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension ZHTopHundredView:UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataTopArray.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("dailyRecommand", forIndexPath: indexPath) as! ZHGiftCollectionCell
        cell.TopHunderModel = dataTopArray[indexPath.row]
        return cell
    }

}
class ZHTopHundredLayout: UICollectionViewFlowLayout {
    override func prepareLayout() {
        itemSize = CGSizeMake(kScreenWidth / 2 - 10, kScreenWidth / 2 * 1.35)
        scrollDirection = UICollectionViewScrollDirection.Vertical
        minimumLineSpacing = 10
        minimumInteritemSpacing = 0
        sectionInset = UIEdgeInsetsMake(5, 5, 0, 5)
    }
}