//
//  ZHGiftCollectionCell.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/9.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHGiftCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var DailyModel : ZHDailyRecommandModel? {
        didSet{
          imageV.sd_setImageWithURL(DailyModel?.cover_image_url)
          titleLabel.text = DailyModel?.short_description
          nameLabel.text = DailyModel?.name

          priceLabel.text = "¥ " + DailyModel!.price!
        }
    }
    
    var TopHunderModel : ZHTopHunderModel? {
        didSet{
            imageV.sd_setImageWithURL(TopHunderModel?.cover_image_url)
            titleLabel.text = TopHunderModel?.short_description
            nameLabel.text = TopHunderModel?.name
            
            priceLabel.text = "¥ " + TopHunderModel!.price!
        }
    }
    var AloneModel : ZHAloneOriginalModel? {
        didSet{
            imageV.sd_setImageWithURL(AloneModel?.cover_image_url)
            titleLabel.text = AloneModel?.short_description
            nameLabel.text = AloneModel?.name
            
            priceLabel.text = "¥ " + AloneModel!.price!
        }
    }
    var NewsModel : ZHNewsStarModel? {
        didSet{
            imageV.sd_setImageWithURL(NewsModel?.cover_image_url)
            titleLabel.text = NewsModel?.short_description
            nameLabel.text = NewsModel?.name
            
            priceLabel.text = "¥ " + NewsModel!.price!
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.layer.borderWidth = 0.5
    }
    
    override func layoutSubviews() {
        
    }
    
}
