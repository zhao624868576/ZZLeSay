//
//  ZHItemViewController.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/10.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit
import SystemConfiguration

class ZHItemViewController: UIViewController {

    var dataArray = [ZHItemModel]() {
        didSet{
            self.leftView.leftDataArray = self.dataArray
            self.rightView.rightDataArray = self.dataArray
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        ZHNetworkTools.shareInstance.loadCategoryItemData { (result, error) in
            if error != nil{
              return
            }
            for dict in result!{
             let model = ZHItemModel(dict: dict)
                self.dataArray.append(model)
            }
            self.view.addSubview(self.leftView)
            self.view.addSubview(self.rightView)
        }
    }
    //左边tableview
    private lazy var leftView : ZHLeftTableView = {
        let left = ZHLeftTableView()
        left.frame = CGRectMake(0, 0, kScreenWidth / 4, kScreenHeight - 64 - 49 - 50)
        return left
    }()
    //右边tableview
    private lazy var rightView : ZHRightTableView = {
        let right = ZHRightTableView(frame: CGRectMake(kScreenWidth / 4, 0, kScreenWidth / 4 * 3, kScreenHeight - 64 - 49 - 50))
        return right
    }()
}
extension ZHItemViewController {

}