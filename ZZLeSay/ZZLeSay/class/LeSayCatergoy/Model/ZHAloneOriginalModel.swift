//
//  ZHAloneOriginalModel.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/13.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHAloneOriginalModel: NSObject {

    var cover_image_url : NSURL?      //图片
    var short_description : String?   //简短描述
    var name : String?                //名称
    var price : String?               //价格
    var url : String?                 //跳转URL
    
    init(dict : [String : AnyObject]) {
        super.init()
        
        setValuesForKeysWithDictionary(dict)
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) { }
}
