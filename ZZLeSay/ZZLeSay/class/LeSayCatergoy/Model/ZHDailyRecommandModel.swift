//
//  ZHDailyRecommandModel.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/9.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHDailyRecommandModel: NSObject {

    /*
     "ad_monitors": null,
     "brand_id": null,
     "brand_order": null,
     "category_id": 4,
     "cover_image_url": "http://img01.liwushuo.com/image/151015/ykhbb4nxr_w.jpg-w720",
     "cover_webp_url": "http://img01.liwushuo.com/image/151015/ykhbb4nxr_w.jpg?imageView2/2/w/720/q/85/format/webp",
     "created_at": 1444888541,
     "description": "女生就应该24小时1年四季喝温水，才是对身体最好的保养。这个大大的保温桶，喝水、闷粥都可以，还可以塞点薏米红豆闷一晚上，第二天就能喝到健康的养生糖水了。",
     "editor_id": 1037,
     "favorites_count": 3441,
     "id": 1036825,
     "image_urls": [],
     "keywords": "",
     "name": "tanana·不锈钢保温桶",
     "order": 1,
     "post_ids": [ ],
     "price": "129.00",
     "purchase_id": "522984310599",
     "purchase_shop_id": null,
     "purchase_status": 1,
     "purchase_type": 1,
     "purchase_url": "http://s.click.taobao.com/t?e=m%3D2%26s%3DpEvaLcdOUH8cQipKwQzePOeEDrYVVa64XoO8tOebS%2BdRAdhuF14FMc2NNS7zx6MGRitN3%2FurF3ytgmtnxDX9deVMA5qBABUs5mPg1WiM1P5OS0OzHKBZzW1e2y4p13L5sj1wD0FohoUgN7ngj44Xl%2FaW9A6%2FIHgl%2BUUq5xoumkE%3D",
     "short_description": "随时随地的温暖",
     "subcategory_id": 30,
     "updated_at": 1444888541,
     "url": "http://www.liwushuo.com/items/1036825"
     */
    var cover_image_url : NSURL?      //图片
    var short_description : String?   //简短描述
    var name : String?                //名称
    var price : String?               //价格
    var url : String?                 //跳转URL
    
    init(dict : [String : AnyObject]) {
        super.init()
        
        setValuesForKeysWithDictionary(dict)
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) { }
    
}


