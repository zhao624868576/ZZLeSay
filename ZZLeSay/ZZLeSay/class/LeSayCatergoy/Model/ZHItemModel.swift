//
//  ZHItemModel.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/7.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHItemModel: NSObject {
   /*
    "icon_url": "http://img02.liwushuo.com/image/150615/3nc5tejwl.png-pw144",
    "id": 1,
    "name": "热门分类",
    "order": 11,
    "status": 0,
    "subcategories": []
    */
    var icon_url : NSURL?                     //类别配图
    var name : String?                         //类别名称
    var status : Float = 0                     //类别状态
    var subcategories : [[String : AnyObject]]?//存放子类别字典数组
    var subItem : ZHSubItemModel?
    var tempArray = [ZHSubItemModel]()
    
    
    var id : Float = 0
    var order : Float = 0
    
    init(dict:[String : AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
        
        // 1.将用户字典转成用户模型对象
        if let subDicts = dict["subcategories"] as? [[String : AnyObject]] {
            for dict in subDicts {
                subItem = ZHSubItemModel(dict: dict)
                tempArray.append(subItem!)
            }
        }
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
    }
}
