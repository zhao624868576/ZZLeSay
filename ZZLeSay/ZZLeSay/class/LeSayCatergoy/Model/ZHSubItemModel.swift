//
//  ZHSubItemModel.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/7.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHSubItemModel: NSObject {
    
    /*
     "icon_url": "http://img02.liwushuo.com/image/150615/urgs9vy8a.png-pw144",
     "id": 7,
     "items_count": -52,
     "name": "智能设备",
     "order": 7,
     "parent_id": 1,
     "status": 0
     */
    var icon_url : NSURL?
//    var items_count : String?
    var name : String?
    
    
    
    
    init(dict:[String : AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)

        }
    
     override func setValue(value: AnyObject?, forUndefinedKey key: String) {
    }
}
