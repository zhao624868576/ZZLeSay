//
//  ZHSectionView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/3.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHSectionView: UIView {

    class func createSectionView() -> ZHSectionView{
        return NSBundle.mainBundle().loadNibNamed("ZHSectionView", owner: nil, options: nil).first as! ZHSectionView
    }
    @IBOutlet weak var indicationLeftContraints: NSLayoutConstraint!
    
    @IBAction func clickProduce(sender: AnyObject) {
            
            self.indicationLeftContraints.constant = 0

    }
    @IBAction func clickStrategy(sender: AnyObject) {
        
            self.indicationLeftContraints.constant = self.frame.size.width * 0.5

    }
}
