//
//  ZHMeOptionView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/3.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHMeOptionView: UIView {

    
    var btns = [UIButton]()
    let lineView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.grayColor()
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setUI()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setUIFrame()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func setUI(){
        backgroundColor = UIColor.whiteColor()
        addSubview(createBtn("shopcart_bt", title: "购物车"))
        addSubview(createBtn("order_bt", title: "订单"))
        addSubview(createBtn("discount_bt", title: "礼劵"))
        addSubview(createBtn("costomer_bt", title: "客服"))
        
        addSubview(lineView)
    }
    private func setUIFrame(){
        var i = 0
        let btnW = frame.width / 4
        let btnH = frame.height
        for btn in btns {
            btn.frame = CGRectMake(CGFloat(i) * btnW, 0, btnW, btnH)
            btn.tag = i
            i = i + 1
        }
        lineView.snp_makeConstraints { (make) in
            make.height.equalTo(1)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(0)
        }
    }
    private func createBtn(imageName:String,title:String)->UIButton{
        let button = ZHMeTitleBtn()
        button.setTitle(title, forState: .Normal)
        button.setImage(UIImage.init(named: imageName), forState: .Normal)
        button.setImage(UIImage.init(named: imageName), forState: .Highlighted)
        button.titleLabel?.font = UIFont.systemFontOfSize(12)
        button.setTitleColor(UIColor.grayColor(), forState: .Normal)
        btns.append(button)
        return button
    }
}
