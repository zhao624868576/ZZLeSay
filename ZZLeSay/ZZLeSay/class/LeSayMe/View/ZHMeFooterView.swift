//
//  ZHMeFooterView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/3.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHMeFooterView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUI()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setUIFrame()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func setUI(){
        addSubview(icon)
        addSubview(tipLabel)
    }
    private func setUIFrame(){
       tipLabel.snp_makeConstraints { (make) in
          make.centerX.equalTo(self.snp_centerX)
          make.centerY.equalTo(self.snp_centerY)
        }
        icon.snp_makeConstraints { (make) in
            make.centerX.equalTo(self.snp_centerX)
            make.bottom.equalTo(tipLabel.snp_top).offset(-10)
        }
    }
    private let icon : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.init(named: "me_blank")
        return imageView
    }()
    private let tipLabel : UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFontOfSize(15)
        label.text = "登录以享受更多功能"
        label.textColor = UIColor.grayColor()
        label.textAlignment = .Center
        return label
    }()
    
}
