//
//  ZHMeHeaderView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/3.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit
import SnapKit

class ZHMeHeaderView: UIView {

   override init(frame: CGRect) {
        super.init(frame: frame)
        self.setUI()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setUIFrame()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func setUI(){
        backgroundColor = UIColor.whiteColor()
        self.addSubview(bgimageView)
        self.addSubview(loginBtn)
        self.addSubview(userName)
        self.addSubview(caView)
        loginBtn.addTarget(self, action: #selector(self.clickLoginAction), forControlEvents: .TouchUpInside)
    }
    private func setUIFrame(){
        loginBtn.snp_makeConstraints { (make) in
            make.centerX.equalTo(self.snp_centerX)
            make.top.equalTo(self.snp_top).offset(80)
        }
        userName.snp_makeConstraints { (make) in
            make.centerX.equalTo(self.snp_centerX)
            make.top.equalTo(loginBtn.snp_bottom).offset(5)
        }
        caView.snp_makeConstraints { (make) in
            make.left.equalTo(0)
            make.bottom.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(self.snp_width)
            make.height.equalTo(70.0)
        }
    }
    //懒加载
    //头部背景图片
    lazy var bgimageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "me_profilebackground"))
        return imageView
    }()
    private lazy var loginBtn : UIButton = {
        let logBtn = UIButton()
        logBtn.setImage(UIImage.init(named: "me_avatar_boy"), forState: .Normal)
        logBtn.sizeToFit()
        return logBtn
    }()
    lazy var userName : UILabel = {
        let loglabel = UILabel()
        loglabel.text = "登录"
        loglabel.font = UIFont.systemFontOfSize(14)
        loglabel.textColor = UIColor.whiteColor()
        loglabel.textAlignment = .Center
        loglabel.sizeToFit()
        return loglabel
    }()
    lazy var caView : ZHMeOptionView = {
        let view = ZHMeOptionView()
        return view
    }()
}
extension ZHMeHeaderView{
    
    @objc func clickLoginAction(){
    
    }
    
}
