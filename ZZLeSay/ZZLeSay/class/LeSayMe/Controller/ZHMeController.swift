//
//  ZHMeController.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/7/26.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit
import SnapKit

class ZHMeController: UIViewController {

    var tableView : UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
     self.view.backgroundColor = UIColor.whiteColor()
        self.setNavUI()
        self.setContentUI()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.translucent = true
    }
  //设置导航栏
    private func setNavUI(){

        self.automaticallyAdjustsScrollViewInsets = false
        let messageBtn = UIButton.init(x: -20, iconName:"me_message", target: self, selector: #selector(self.clickMessageBtn))
        let alarmclockBtn = UIButton.init(x: 24, iconName: "me_giftremind", target: self, selector: #selector(self.clickalarmclockBtn))
        leftView.addSubview(messageBtn)
        leftView.addSubview(alarmclockBtn)
        
        let scanBtn = UIButton.init(x: 0, iconName:"me_scan", target: self, selector: #selector(self.clickScanBtn))
        let settingBtn = UIButton.init(x: 44, iconName: "iconSettings", target: self, selector: #selector(self.clicSettingBtn))
        rightView.addSubview(scanBtn)
        rightView.addSubview(settingBtn)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: leftView)
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: rightView)
        
    }
    //添加中间滚动视图
    private func setContentUI(){
        tableView = UITableView(frame: view.bounds, style: .Grouped)
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.sectionFooterHeight = 0
        tableView?.sectionHeaderHeight = 0
        view.addSubview(tableView!)
        tableView?.showsVerticalScrollIndicator = false
        tableView?.tableHeaderView = headerView
        tableView?.tableFooterView = footerView
        
    }
    private lazy var footerView : ZHMeFooterView = {
         let view = ZHMeFooterView()
         view.frame = CGRectMake(0, 0, kScreenWidth, 300)
         return view
    }()
    private lazy var headerView : ZHMeHeaderView = {
        let view = ZHMeHeaderView()
        view.frame = CGRectMake(0, 0, kScreenWidth, 277)
        return view
    }()
    lazy var leftView : UIView = {
       let view = UIView()
        view.frame = CGRectMake(0, 0, 88, 44)
        return view
    }()
    lazy var rightView : UIView = {
        let view = UIView()
        view.frame = CGRectMake(0, 0, 88, 44)
        return view
    }()
    private lazy var sectionView: ZHSectionView = {
        let view = ZHSectionView.createSectionView()
        view.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 60)
        return view
    }()
}
//点击方法
extension ZHMeController{
   //点击nav信封按钮
    @objc func clickMessageBtn(){
        
    }
    //点击nav闹钟按钮
    @objc func clickalarmclockBtn(){
        
    }
    //点击nav扫码按钮
    @objc func clickScanBtn(){
        
    }
    //点击nav设置按钮
    @objc func clicSettingBtn(){
        self.navigationController?.pushViewController(ZHMeSetViewController(), animated: true)
    }
}
//tableView代理方法
extension ZHMeController:UITableViewDataSource,UITableViewDelegate{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let ID : String = "cell"
        var cell = tableView.dequeueReusableCellWithIdentifier(ID)
        if cell == nil {
            cell = UITableViewCell.init(style: .Default, reuseIdentifier: ID)
        }
        return cell!
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return sectionView
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        //处理图片拉伸
        let offsetY = scrollView.contentOffset.y
        if offsetY > 0 {
            return
        }
        let upFactor : CGFloat = 0.6
        if offsetY >= 0.0 {
            headerView.bgimageView.transform = CGAffineTransformMakeTranslation(0, offsetY * upFactor)
        }else{
            let transform = CGAffineTransformMakeTranslation(0, offsetY)
            let s = 1 - offsetY * 0.01
            headerView.bgimageView.transform = CGAffineTransformScale(transform, 1, s)
            
        }
    }
}