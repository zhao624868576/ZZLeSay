//
//  ZHMeSetViewController.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/14.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

    
    class ZHMeSetViewController: UIViewController {
        
        let sectionArr : Array = [[["me","我的身份"]],[["invite","邀请好友使用礼物说"],["grade","给我们评分吧"],["suggest","意见反馈"],["service","客服电话"]],[["like","喜欢单品到默认清单"],["msgpush","消息推送设置"],["cleancache","清除缓存"],["netmonitor","网络监测"]],[["about","关于礼物说"]]]
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.title = "设置"
            self.view.backgroundColor = UIColor.whiteColor()
            
            setUI()
        }
        override func viewDidAppear(animated: Bool) {
            super.viewDidAppear(animated)
            self.navigationController?.navigationBar.backgroundColor = Color_NavBackground
        }
        private func setUI(){
            let tablView = UITableView(frame: CGRectMake(0, 0, kScreenWidth, kScreenHeight), style: UITableViewStyle.Plain)
            tablView.delegate = self
            tablView.dataSource = self
            self.view.addSubview(tablView)
            
        }
    }
    extension ZHMeSetViewController:UITableViewDataSource,UITableViewDelegate{
        
        func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return sectionArr.count
        }
        
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            let row = sectionArr[section]
            return row.count
        }
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            var cell = tableView.dequeueReusableCellWithIdentifier("cell")
            if  cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
            }
            let section = sectionArr[indexPath.section]
            let row = section[indexPath.row]
            
            cell?.textLabel?.text = row.last
            cell?.detailTextLabel?.text = row.first
            return cell!
        }
        func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 20
        }
    }
    extension ZHMeSetViewController {
        
        
}
