//
//  ZHLoginController.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/7/28.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHLoginController: UIViewController {
    
    @IBOutlet weak var phoneTextField: UITextField!      //手机号输入框
    
    @IBOutlet weak var enterPasswordTextField: UITextField!  //密码输入/验证码输入框
    
    @IBOutlet weak var getVerMessageBtn: UIButton!   //获取验证码按钮
    
    @IBOutlet weak var changeLoginBtn: UIButton!   //更改登录方式按钮
    
    @IBOutlet weak var loginBtn: UIButton!       //登录按钮
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginBtn.layer.cornerRadius = 8
        self.loginBtn.layer.masksToBounds = true
    }
}

//分类,点击事件
extension ZHLoginController{
    //点击删除按钮, 退出登录界面
    @IBAction func clickDeleteBtn(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    //点击获取短信码按钮, 获取短信验证码
    @IBAction func clickGetMessageBtn(sender: UIButton) {
        
    }
    //点击登录按钮,发送登录请求
    @IBAction func clickLoginBtn(sender: UIButton) {
        
    }
    //点击改变登录方式按钮, 密码登录->短信验证登录
    @IBAction func clickChangLogBtn(sender: UIButton) {
        
    }
    //第三方微信登录
    @IBAction func loginWechat(sender: UIButton) {
        print("微信登录---loginWechat")
    }
    //第三方新浪登录
    @IBAction func loginSina(sender: UIButton) {
        print("新浪登录---loginWechat")
    }
    //第三方qq登录
    @IBAction func loginQQ(sender: UIButton) {
        print("qq登录---loginWechat")
    }
}
