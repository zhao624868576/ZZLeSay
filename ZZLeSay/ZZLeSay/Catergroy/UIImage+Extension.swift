//
//  UIImage+Extension.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/7/28.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

extension  UIImage{
 
    class func imageWithColor(color:UIColor, size:CGSize)->UIImage? {
        let rect = CGRectMake(0, 0, size.width == 0 ? 1.0 : size.width, size.height == 0 ? 1.0 : size.height)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image : UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
  
}
