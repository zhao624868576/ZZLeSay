//
//  UIButton+Extension.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/7/28.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit


extension UIButton{
  
    //导航栏功能按钮(我的界面)
    convenience init(x:CGFloat,iconName:String,target:AnyObject,selector:Selector){
        self.init()
        
        setImage(UIImage.init(named: iconName), forState: .Normal)
        setImage(UIImage.init(named: iconName), forState: .Highlighted)
        frame = CGRectMake(x, 0, 44, 44)
        addTarget(target, action: selector, forControlEvents: .TouchUpInside)
        contentHorizontalAlignment = .Right
        
    }
    //导航栏返回按钮
    convenience init(navBacktarget:AnyObject,selector:Selector){
        self.init()
        setImage(UIImage.init(named: "back"), forState: .Normal)
        frame = CGRectMake(0, 0, 44, 44)
        addTarget(navBacktarget, action: selector, forControlEvents: .TouchUpInside)
        contentHorizontalAlignment = .Right
        
    }
}
