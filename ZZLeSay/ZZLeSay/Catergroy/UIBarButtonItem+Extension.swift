//
//  UIBarButtonItem+Extension.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/7/28.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

extension UIBarButtonItem{
   
    //主页,点击左边的按钮
    convenience init(gifTarget:AnyObject?,seletor:Selector) {
        let packView = UIView.init(frame: CGRectMake(0, 20, 44, 24))
        let btn = UIButton(type: .Custom)
        btn.setImage(UIImage.init(named: "feed_signin"), forState: .Normal)
        btn.sizeToFit()
        btn.contentHorizontalAlignment = .Left
        btn.addTarget(gifTarget, action: seletor, forControlEvents: .TouchUpInside)
        packView.addSubview(btn)
        self.init(customView: packView)
    }
    
    //主页,点击右边的按钮
    convenience init(searchTarget:AnyObject?,seletor:Selector) {
        let packView = UIView.init(frame: CGRectMake(30, 20, 24, 24))
        let btn = UIButton(type: .Custom)
        btn.setImage(UIImage.init(named: "icon_navigation_search"), forState: .Normal)
        btn.sizeToFit()
        btn.contentHorizontalAlignment = .Right
        btn.addTarget(searchTarget, action: seletor, forControlEvents: .TouchUpInside)
        packView.addSubview(btn)
        self.init(customView: packView)
    }
    
    //分类,点击右边的 选礼神器 按钮
    convenience init(Target:AnyObject?,seletor:Selector) {
        let packView = UIView.init(frame: CGRectMake(0, 0, 80, 24))
        let button = UIButton()
        button.setTitle("选礼神器", forState: .Normal)
        button.titleLabel?.font = UIFont.systemFontOfSize(15)
        button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        button.addTarget(Target, action: seletor, forControlEvents: .TouchUpInside)
        button.frame = packView.bounds
        packView.addSubview(button)
        self.init(customView: packView)
    }
    //热点界面,点击右边的按钮
    convenience init(seachTarget:AnyObject?,seletor:Selector) {
        let packView = UIView.init(frame: CGRectMake(0, 20, 44, 24))
        let btn = UIButton(type: .Custom)
        btn.setImage(UIImage.init(named: "icon_navigation_search"), forState: .Normal)
        btn.sizeToFit()
        btn.contentHorizontalAlignment = .Left
        btn.addTarget(seachTarget, action: seletor, forControlEvents: .TouchUpInside)
        packView.addSubview(btn)
        self.init(customView: packView)
    }
}
