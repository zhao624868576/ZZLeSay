//
//  ZHBeziderView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/9/1.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHBeziderView: UIView {

    
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        
        let width = rect.size.width
        let height = rect.size.height
        
        UIColor.lightGrayColor().set()
        
        let path = UIBezierPath()
        
        path.moveToPoint(CGPointMake(width/4, 0))
        path.addLineToPoint(CGPointMake(width/4, height))
        
        path.moveToPoint(CGPointMake(width/2, 0))
        path.addLineToPoint(CGPointMake(width/2, height))
        
        path.moveToPoint(CGPointMake(width/4 * 3, 0))
        path.addLineToPoint(CGPointMake(width/4 * 3, height / 4 * 3))
        
        path.moveToPoint(CGPointMake(0, height / 4))
        path.addLineToPoint(CGPointMake(width, height / 4))
        
        path.moveToPoint(CGPointMake(0, height / 2))
        path.addLineToPoint(CGPointMake(width, height/2))
        
        path.moveToPoint(CGPointMake(0, height/4 * 3))
        path.addLineToPoint(CGPointMake(width, height/4 * 3))
        
        path.lineWidth = 0.8
        path.stroke()
        
    }
 

}
