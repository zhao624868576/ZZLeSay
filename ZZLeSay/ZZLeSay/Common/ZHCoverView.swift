//
//  ZHCoverView.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/1.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHCoverView: UIView {

    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
        backgroundColor = UIColor(white: 0.0, alpha: 0.6)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
