//
//  ZHBaseNavigationController.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/7/26.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHBaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavUI()

    }
    private func setNavUI(){
    
        self.navigationBar.setBackgroundImage(UIImage.imageWithColor(Color_NavBackground, size: CGSizeMake(1, 1)), forBarMetrics: .Default)
    
        var textAtt : [String:AnyObject] = Dictionary()
        textAtt[NSFontAttributeName] = UIFont.systemFontOfSize(20)
        textAtt[NSForegroundColorAttributeName] = UIColor.whiteColor()
        self.navigationBar.titleTextAttributes = textAtt
    }
    
    lazy var backButton : UIButton = {
          let btn = UIButton(navBacktarget: self, selector: #selector(ZHBaseNavigationController.backAction))
          btn.frame = CGRectMake(0,0,8,20)
          btn.contentHorizontalAlignment = .Left
          btn.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0)
          return btn
    }()
    
    func backAction(){
        self.popViewControllerAnimated(true)
    }
    
    override func pushViewController(viewController: UIViewController, animated: Bool) {
        if self.childViewControllers.count > 0 {
            viewController.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backButton)
            viewController.hidesBottomBarWhenPushed = true
        }
        super.pushViewController(viewController, animated: animated)
    }
}
