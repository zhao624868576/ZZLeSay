//
//  ZHBaseTabBarController.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/7/26.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHBaseTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
     //添加所有子控制器
    addChildViewControllers()
    }
    private func addChildViewControllers(){
        addChildViewController(ZHMainController(), title: "礼物说", imageName: "tabbar_home")
        addChildViewController(ZHHotController(), title: "热门", imageName: "tabbar_gift")
        addChildViewController(ZHCatergroyController(), title: "分类", imageName: "tabbar_category")
        addChildViewController(ZHMeController(), title: "我", imageName: "tabbar_me")
    }
    //添加单个子控制器
    private func addChildViewController(childController: UIViewController,title : String ,imageName : String){
        childController.tabBarItem.image = UIImage(named: imageName)
        childController.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName :UIColor.redColor()], forState: .Selected)
        childController.tabBarItem.title = title
        childController.tabBarItem.selectedImage = UIImage(named: imageName + "_selected")
//        childController.title = title;
        let nav = ZHBaseNavigationController()
        nav.addChildViewController(childController)
        addChildViewController(nav)
    }

}
