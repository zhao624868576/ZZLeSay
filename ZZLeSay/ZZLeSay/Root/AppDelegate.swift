//
//  AppDelegate.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/7/26.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,PushManagerDelegate{

    var window: UIWindow?
    static var  ActiveFlag : Bool = false
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        //创建窗口
        window = UIWindow.init(frame: UIScreen.mainScreen().bounds)
        window?.rootViewController = ZHLauchViewController()
        window?.makeKeyAndVisible()
        
        
/**
 开启推送服务并设置推送代理以及DeviceToken回调代理、自定义展示页面数据
 pushDelegate  推送代理，必须，参数对象必须实现onMessage:content:extention:方法
 deviceTokenDelegate  DeviceToken获取代理，如果无需获取SDK中得DeviceToken，此项可为nil。
 */
    PushManager.startPushServicePushDelegate(self, tokenDelegate: nil)
    
        return true
    }
    
    func onMessage(title: String!, content: String!, extention: [NSObject : AnyObject]!) -> Bool {
        /*
        代理回调方法
        
        @param title     消息title内容
        @param content   消息content内容
        @param extention 参数字典,此字典包含用户自定义参数和应用状态参数。
        获取应用状态参数可通过 AppStateKey 来获取，状态类型参见  UIApplicationState
        
        @return BOOL 当返回YES时，仅处理至当前事件处，后续事件将不再执行
        当返回NO时，按照事件链继续执行，直至返回YES或者所有事件执行完。
        */
        print(title)
        print(content)
        print(extention)
        
        let actionUrl = extention["urlID"]
        print(actionUrl)
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        
    }

    func applicationDidEnterBackground(application: UIApplication) {
    }

    
    func applicationWillEnterForeground(application: UIApplication) {

    }
    func applicationDidBecomeActive(application: UIApplication) {
        

        
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

