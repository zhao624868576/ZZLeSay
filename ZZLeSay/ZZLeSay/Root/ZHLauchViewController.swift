//
//  ZHLauchViewController.swift
//  ZZLeSay
//
//  Created by 赵正华 on 16/8/18.
//  Copyright © 2016年 赵正华. All rights reserved.
//

import UIKit

class ZHLauchViewController: UIViewController {

    private var images : [UIImageView] = {
         let arr = [UIImageView]()
         return arr
    }() //图片数组
    private let count : Int = 24
    private var index = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.orangeColor()
        //添加背景图片
        self.addBackImage()
        //添加数组图片
        self.addAnimationImage()
        //开启定时器动画
        NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(self.startAnimation(_:)), userInfo: nil, repeats: true)
    }
    @objc func startAnimation(timer : NSTimer){
        if index == images.count {
            timer.invalidate()
            UIApplication.sharedApplication().keyWindow?.rootViewController = ZHBaseTabBarController()
        }else{
        
            UIView.animateWithDuration(0.25, animations: {
                 let imageV = self.images[self.index]
                 imageV.alpha = 1
            })
        }
        self.index += 1
    }
    private func addBackImage(){
        let backImage = UIImageView.init()
        backImage.image = UIImage.init(named: "LaunchImage.png")
        backImage.frame = view.bounds
        view.addSubview(backImage)
    }
    private func addAnimationImage(){
        let imageH = kScreenHeight / 6
        let imageW = kScreenWidth / 4
        var originX : CGFloat = 0
        var originY : CGFloat = 0
        for i in 0..<count {
            let imageV = UIImageView.init()
            if i > 16 {
                imageV.image = UIImage.init(named:"goods_\(i - 16).jpg" )
            }else{
            
                imageV.image = UIImage.init(named:"strategy_\(i+1).jpg" )
            }
            imageV.frame = CGRectMake(originX, originY, imageW, imageH)
            imageV.alpha = 0
            
            if i < 4 {
                originX = CGFloat(i) * imageW
                originY = 0
            }else if i < 8{
                originX = kScreenWidth - imageW
                originY = CGFloat(i - 3) * imageH
            }else if i < 12{
                originX = kScreenWidth - CGFloat(i - 7) * imageW
                originY = kScreenHeight - imageH
            }else if i < 16{
                originX = 0
                originY = kScreenHeight - CGFloat(i - 10) * imageH
            }else if i < 18{
                originX = CGFloat(i - 15) * imageW
                originY = imageH
            }else if i < 21{
                originX = imageW * 2
                originY = imageH * 2 + CGFloat(i - 18) * imageH
            }else if i < 24{
                originX = imageW
                originY = kScreenHeight - CGFloat(i - 19) * imageH
            }
            imageV.frame.origin = CGPointMake(originX, originY)
            view.addSubview(imageV)
            images.append(imageV)
        }
    }
}
